<?php

declare(strict_types=1);

namespace Faker\Philippines;

use Faker\Generator;

class Factory extends \Faker\Factory
{
    public static function philippines(): Generator
    {
        $generator = new Generator();

        foreach (static::$defaultProviders as $provider) {
            $providerClassName = static::getProviderClassname($provider);
            $generator->addProvider(new $providerClassName($generator));
        }

        return $generator;
    }

    /**
     * {@inheritDoc}
     */
    protected static function getProviderClassname($provider, $locale = 'en_PH')
    {
        $providerClass = 'Faker\\Philippines\\' . $provider;

        if (class_exists($providerClass, true)) {
            return $providerClass;
        }

        return parent::getProviderClassname($provider, $locale);
    }
}
